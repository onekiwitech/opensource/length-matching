import pcbnew
import os

class KiCad():
    def __init__(self, board):
        self.board = board

    def GetPcbName(self):
        file_name = str(self.board.GetFileName())
        base = os.path.basename(file_name)
        name = os.path.splitext(base)[0]
        return name
    
    def GetPcbFullName(self):
        file_name = str(self.board.GetFileName())
        name = os.path.basename(file_name)
        return name

    def GetPcbPath(self):
        file_name = str(self.board.GetFileName())
        path = os.path.dirname(file_name)
        return path

    def GetPluginPath(self):
        # log_file = os.path.join(os.path.dirname(__file__), "..", "lengthmatching.log")
        current_path = os.path.dirname(__file__)
        path = os.path.dirname(current_path)
        return path