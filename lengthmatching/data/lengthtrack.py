import logging
import pcbnew
import logging

def get_lenght(start_ref, start_pin, end_ref, end_pin):
    board = pcbnew.GetBoard()
    start_pad = board.FindFootprintByReference(start_ref).FindPadByNumber(start_pin)
    end_pad = board.FindFootprintByReference(end_ref).FindPadByNumber(end_pin)
    net_name = start_pad.GetNetname()
    net_code = board.GetNetcodeFromNetname(net_name)
    tracks = list(board.TracksInNet(net_code)) #Convert Tuple to List
    start_pad_layer = board.FindFootprintByReference(start_ref).IsFlipped()
    end_pad_layer = board.FindFootprintByReference(end_ref).IsFlipped()

    current_point = start_pad.GetPosition()
    current_layer = 'Any'
    if start_pad.GetAttribute() == pcbnew.PAD_ATTRIB_SMD:
        if start_pad_layer == True:
            # F_Cu = 31
            current_layer = pcbnew.B_Cu
        else:
            # F_Cu = 0
            current_layer = pcbnew.F_Cu

    end_point = end_pad.GetPosition()
    end_layer = 'Any'
    if end_pad.GetAttribute() == pcbnew.PAD_ATTRIB_SMD:
        if end_pad_layer == True:
            end_layer = pcbnew.B_Cu
        else:
            end_layer = pcbnew.F_Cu

    isLoop = True
    isEnd = True
    totals = []
    while (len(tracks)) > 0 and (isLoop == True) and (isEnd == True):
        isEnd = False
        for track in tracks:
            if track not in totals:
                if track.GetClass() == 'PCB_VIA':
                    if current_point == track.GetPosition():
                        current_layer = 'Any'
                        totals.append(track)
                        isEnd = True
                else:
                    ponit_start = track.GetStart()
                    ponit_end = track.GetEnd()
                    track_layer = track.GetLayer()
                    if current_point == ponit_start and (current_layer == track_layer or current_layer == 'Any'):
                        current_point = ponit_end
                        current_layer = track_layer
                        totals.append(track)
                        isEnd = True
                    elif current_point == ponit_end and (current_layer == track_layer or current_layer == 'Any'):
                        current_point = ponit_start
                        current_layer = track_layer
                        totals.append(track)
                        isEnd = True
                    
                if current_point == end_point and (current_layer == end_layer or current_layer == 'Any'):
                    isLoop = False

    sum = 0.0
    for total in totals:
        if total.GetClass() == 'PCB_TRACK':
            sum += total.GetLength()
    length = round(sum/pcbnew.IU_PER_MM, 4)
    return length

def get_track_length(netname):
    board = pcbnew.GetBoard()
    netcode = board.GetNetcodeFromNetname(netname)
    tracks = board.TracksInNet(netcode)
    sum = 0.0
    for track in tracks:
        if track.GetClass() != "VIA":
            sum += track.GetLength()
    length = round(sum/pcbnew.IU_PER_MM, 4)
    return length