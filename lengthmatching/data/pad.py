import pcbnew

def get_pad_by_reference(reference, pin):
    board = pcbnew.GetBoard()
    pad = board.FindFootprintByReference(reference).FindPadByNumber(pin)
    return pad

def get_net_name_from_pad(pad):
    netname = pad.GetNetname()
    return netname

def get_position_pad(pad):
    position = pad.GetPosition()
    return position

def get_attribute_pad(pad):
    attribute = pad.GetAttribute()
    return attribute