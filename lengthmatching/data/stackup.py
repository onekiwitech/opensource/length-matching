import pcbnew
import json
import logging
from .pcb import *


class Stackup:
    def __init__(self, name, thickness):
        self.name = name
        self.thickness = thickness

coppers = []
dielectrics = []

# get layer stack up
def create_job_file():
    board = get_board()
    path = get_pcb_path()
    job_file = os.path.join(path, "jobfile.json")
    pcbnew.GERBER_JOBFILE_WRITER(board).CreateJobFile(job_file)
    pcbnew.GERBER_JOBFILE_WRITER(board).WriteJSONJobFile(job_file)
    return job_file


def read_job_file(file):
    coppers.clear()
    dielectrics.clear()
    # Opening JSON file
    f = open(file)
  
    # returns JSON object as 
    # a dictionary
    data = json.load(f)
  
    # Iterating through the json
    # list
    for obj in data['MaterialStackup']:
        if obj['Type'] == 'Copper':
            coppers.append(Stackup(obj['Name'], obj['Thickness']))
        
        if obj['Type'] == 'Dielectric':
            dielectrics.append(Stackup(obj['Name'], obj['Thickness']))
    
    # Closing file
    f.close()

    for copper in coppers:
        logging.debug('%s - %s' , copper.name , copper.thickness)
    
    for copper in dielectrics:
        logging.debug('%s - %s' , copper.name , copper.thickness)