# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.grid

###########################################################################
## Class DialogBase
###########################################################################
###########################################################################
## Class LengthMatchingDialog
###########################################################################

class LengthMatchingDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Length Matching", pos = wx.DefaultPosition, size = wx.Size( 800,500 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		boxMain = wx.BoxSizer( wx.VERTICAL )

		self.notebook = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )

		boxMain.Add( self.notebook, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( boxMain )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_INIT_DIALOG, self.OnDialogInit )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnDialogInit( self, event ):
		event.Skip()


###########################################################################
## Class CreatePanelBase
###########################################################################

class CreatePanelBase ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 800,500 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		boxCreate = wx.BoxSizer( wx.VERTICAL )

		sbSizer4 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Create Class" ), wx.HORIZONTAL )

		bSizer22 = wx.BoxSizer( wx.VERTICAL )

		bSizer29 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText15 = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"From PCB Class", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText15.Wrap( -1 )

		bSizer29.Add( self.m_staticText15, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		comboLayoutChoices = []
		self.comboLayout = wx.ComboBox( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboLayoutChoices, 0 )
		bSizer29.Add( self.comboLayout, 1, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer22.Add( bSizer29, 0, wx.EXPAND, 5 )

		bSizer30 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText14 = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Assign to Class", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText14.Wrap( -1 )

		bSizer30.Add( self.m_staticText14, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.editClass = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer30.Add( self.editClass, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer22.Add( bSizer30, 0, wx.EXPAND, 5 )

		bSizer31 = wx.BoxSizer( wx.HORIZONTAL )

		self.buttonAdd = wx.Button( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Add Class", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer31.Add( self.buttonAdd, 1, wx.ALL, 5 )

		self.buttonEdit = wx.Button( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Edit Class", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer31.Add( self.buttonEdit, 1, wx.ALL, 5 )

		self.buttonDelete = wx.Button( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Delete Class", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer31.Add( self.buttonDelete, 1, wx.ALL, 5 )


		bSizer22.Add( bSizer31, 0, wx.EXPAND, 5 )

		self.textStatus = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.textStatus.Wrap( -1 )

		bSizer22.Add( self.textStatus, 1, wx.ALL|wx.EXPAND, 5 )


		sbSizer4.Add( bSizer22, 1, wx.ALL|wx.EXPAND, 5 )

		listClassChoices = []
		self.listClass = wx.ListBox( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, listClassChoices, 0 )
		sbSizer4.Add( self.listClass, 1, wx.ALL|wx.EXPAND, 5 )


		boxCreate.Add( sbSizer4, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer6 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"label" ), wx.VERTICAL )

		bSizer32 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText16 = wx.StaticText( sbSizer6.GetStaticBox(), wx.ID_ANY, u"Plugin Class", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText16.Wrap( -1 )

		bSizer32.Add( self.m_staticText16, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		comboPluginChoices = []
		self.comboPlugin = wx.ComboBox( sbSizer6.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboPluginChoices, 0 )
		bSizer32.Add( self.comboPlugin, 0, wx.ALL, 5 )


		bSizer32.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.buttonUpdate = wx.Button( sbSizer6.GetStaticBox(), wx.ID_ANY, u"Update Class", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.buttonUpdate, 0, wx.ALL, 5 )


		sbSizer6.Add( bSizer32, 0, wx.EXPAND, 5 )

		self.gridClass = wx.grid.Grid( sbSizer6.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )

		# Grid
		self.gridClass.CreateGrid( 1, 4 )
		self.gridClass.EnableEditing( True )
		self.gridClass.EnableGridLines( True )
		self.gridClass.EnableDragGridSize( False )
		self.gridClass.SetMargins( 0, 0 )

		# Columns
		self.gridClass.SetColSize( 0, 120 )
		self.gridClass.SetColSize( 1, 120 )
		self.gridClass.SetColSize( 2, 120 )
		self.gridClass.SetColSize( 3, 120 )
		self.gridClass.EnableDragColMove( False )
		self.gridClass.EnableDragColSize( True )
		self.gridClass.SetColLabelValue( 0, u"Net Name" )
		self.gridClass.SetColLabelValue( 1, u"Start Pin" )
		self.gridClass.SetColLabelValue( 2, u"End Pin" )
		self.gridClass.SetColLabelValue( 3, u"Inner Length" )
		self.gridClass.SetColLabelAlignment( wx.ALIGN_CENTER, wx.ALIGN_CENTER )

		# Rows
		self.gridClass.EnableDragRowSize( True )
		self.gridClass.SetRowLabelAlignment( wx.ALIGN_CENTER, wx.ALIGN_CENTER )

		# Label Appearance

		# Cell Defaults
		self.gridClass.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
		sbSizer6.Add( self.gridClass, 1, wx.ALL|wx.EXPAND, 5 )


		boxCreate.Add( sbSizer6, 1, wx.ALL|wx.EXPAND, 5 )


		self.SetSizer(  boxCreate )
		self.Layout()

		# Connect Events
		self.buttonAdd.Bind( wx.EVT_BUTTON, self.OnAddClick )
		self.buttonEdit.Bind( wx.EVT_BUTTON, self.OnEditClick )
		self.buttonDelete.Bind( wx.EVT_BUTTON, self.OnDeleteClick )
		self.comboPlugin.Bind( wx.EVT_COMBOBOX, self.OnPluginChange )
		self.buttonUpdate.Bind( wx.EVT_BUTTON, self.OnUpdateClick )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnAddClick( self, event ):
		event.Skip()

	def OnEditClick( self, event ):
		event.Skip()

	def OnDeleteClick( self, event ):
		event.Skip()

	def OnPluginChange( self, event ):
		event.Skip()

	def OnUpdateClick( self, event ):
		event.Skip()
