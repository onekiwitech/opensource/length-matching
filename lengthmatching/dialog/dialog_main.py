import logging
import wx
import wx.grid
import os
import pcbnew

from .dialog_base import *
from ..data.netclass import *
from ..data.lengthtrack import *

class DialogMain(LengthMatchingDialog):
    def __init__(self, parent, board, version):
        LengthMatchingDialog.__init__(self, parent)
        self.SetTitle('Length Matching %s' % version)
        self.board = board

        self.createPanel = CreatePanel(self.notebook)
        #self.xnetPanel = xNetPanel(self.notebook)
        #self.assignPanel = AssignPanel(self.notebook)
        #self.settingPanel = SettingPanel(self.notebook)
        #self.displayPanel = DisplayPanel(self.notebook)
        self.notebook.AddPage(self.createPanel, "Create")
        #self.notebook.AddPage(self.xnetPanel, "xNet")
        #self.notebook.AddPage(self.assignPanel, "Assign")
        #self.notebook.AddPage(self.settingPanel, "Setting")
        #self.notebook.AddPage(self.displayPanel, "Display")
    
    def SetSizeHints(self, a, b, c=None):
        if c is not None:
            super(wx.Dialog, self).SetSizeHints(a,b,c)
        else:
            super(wx.Dialog, self).SetSizeHintsSz(a,b)

class NetData:
    def __init__(self, plugin, layout):
        self.plugin = plugin
        self.layout = layout

class CreatePanel(CreatePanelBase):
    def __init__(self, parent):
        CreatePanelBase.__init__(self, parent)
        self.netClassLayout = get_net_classes()
        self.netClassPlugin = []

        self.comboLayout.Append(self.netClassLayout)
        self.comboLayout.SetSelection(0)

    def OnAddClick(self, event):
        classplugin = self.editClass.GetValue()
        if classplugin == '':
            self.textStatus.LabelText = 'Please input text'
        else:
            self.listClass.Clear()
            self.comboPlugin.Clear()
            layout = self.comboLayout.GetValue()
            self.netClassPlugin.append(NetData(classplugin, layout))
            classes = [item.plugin for item in self.netClassPlugin]
            self.listClass.Append(classes)
            self.comboPlugin.Append(classes)
            self.editClass.SetValue('')



    def OnEditClick( self, event ):
        return 0

    def OnDeleteClick( self, event ):
        return 0

    def OnUpdateClick( self, event ):
        lenght = get_lenght('U6', 'A8', 'U3', 'E3')
        self.textStatus.LabelText = str(lenght)

    def OnPluginChange( self, event ):
        #self.textStatus.LabelText = 'Please input text'
        
        rows = self.gridClass.GetNumberRows()
        self.textStatus.LabelText = str(rows)
        self.gridClass.DeleteRows(0,rows)
        ind = self.comboPlugin.GetSelection()
        netclass = self.netClassPlugin[ind].layout
        self.textStatus.LabelText = netclass
        nets = get_net_names(netclass)
        self.gridClass.AppendRows(len(nets))
        self.textStatus.LabelText = str(len(nets))
        self.textStatus.LabelText = 'Start'
        for row, net in enumerate(nets, start=0):   # Python indexes start at zero
            pads = []
            self.gridClass.SetCellValue(row, 0, net)
            pads = get_pads_from_net_name(net)
            pins = [item.pin for item in pads]
            choice_editor = wx.grid.GridCellChoiceEditor(pins, True)
            self.gridClass.SetCellEditor(row, 1, choice_editor)
            self.gridClass.SetCellEditor(row, 2, choice_editor)
            self.gridClass.SetCellValue(row, 1, pins[0])
            self.gridClass.SetCellValue(row, 2, pins[1])
        self.textStatus.LabelText = 'Done'